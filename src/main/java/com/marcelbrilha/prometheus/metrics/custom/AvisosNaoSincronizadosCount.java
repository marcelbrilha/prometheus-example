package com.marcelbrilha.prometheus.metrics.custom;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@Component
public class AvisosNaoSincronizadosCount {

	private Counter avisosNaoSincronizadosCount;
	
	public AvisosNaoSincronizadosCount(MeterRegistry registry) {
		avisosNaoSincronizadosCount = Counter
                .builder("avisosNaoSincronizadosCount")
                .description("Avisos Não Sincronizados Counter")
                .register(registry);
    }
	
	public void setAvisosNãoSincronizados(int quantidade) {
		avisosNaoSincronizadosCount.increment(quantidade);
	}
}
