package com.marcelbrilha.prometheus.metrics.custom;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.MeterRegistry;

@Component
public class VistoriasAtualizadas {

	private AtomicInteger quantidadeVistoriasAtualizadas;
	
	public VistoriasAtualizadas(MeterRegistry registry) {
		quantidadeVistoriasAtualizadas = registry.gauge("vistoriasAtualizadas", new AtomicInteger(0));
	}
	
	public void setQuantidadeVistoriasAtualizadas(int quantidade) {
		quantidadeVistoriasAtualizadas.set(quantidade);
	}
}
