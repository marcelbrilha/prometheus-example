package com.marcelbrilha.prometheus.actuator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.marcelbrilha.prometheus.service.HealthService;

@Component
public class ExampleAPIHealthIndicator implements HealthIndicator {

	@Autowired
	private HealthService healthService;

	@Override
	public Health health() {
		if (healthService.healthCheck()) {
			return Health.up().withDetail("message", "OK").build();
		}

		return Health.down().withDetail("message", "Communication Failed").build();
	}

}
