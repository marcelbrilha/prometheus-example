package com.marcelbrilha.prometheus.client.response;

public class HealthResponse {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
