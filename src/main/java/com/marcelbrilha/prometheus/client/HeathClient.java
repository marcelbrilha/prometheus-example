package com.marcelbrilha.prometheus.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import com.marcelbrilha.prometheus.client.response.HealthResponse;

@Component
@FeignClient(name = "health-client", url = "${app.example.url}")
public interface HeathClient {

	@GetMapping(value = "/health")
	ResponseEntity<HealthResponse> getHealth();

}
