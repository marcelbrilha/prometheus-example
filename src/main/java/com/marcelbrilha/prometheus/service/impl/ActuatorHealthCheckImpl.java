package com.marcelbrilha.prometheus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.marcelbrilha.prometheus.client.HeathClient;
import com.marcelbrilha.prometheus.client.response.HealthResponse;
import com.marcelbrilha.prometheus.service.HealthService;

@Service
public class ActuatorHealthCheckImpl implements HealthService {

	@Autowired
	private HeathClient exampleHealthClient;

	@Override
	public boolean healthCheck() {
		try {
			ResponseEntity<HealthResponse> response = exampleHealthClient.getHealth();

			return response.getStatusCode() == HttpStatus.OK && response.hasBody()
					&& response.getBody().getStatus().equalsIgnoreCase("UP");

		} catch (Exception ex) {
			return false;
		}
	}

}
