package com.marcelbrilha.prometheus.service;

public interface HealthService {

	boolean healthCheck();

}
